package ru.baulina.tm.api.repository;

import ru.baulina.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Long userId, Task task);

    void remove(Long userId, Task task);

    List<Task> findAll(Long userId);

    void clear(Long userId);

    Task findOneById(Long userId, Long id);

    Task findOneByIndex(Long userId, Integer index);

    Task findOneByName(Long userId, String name);

    Task removeOneById(Long userId, Long id);

    Task removeOneByIndex(Long userId, Integer index);

    Task removeOneByName(Long userId, String name);

}
