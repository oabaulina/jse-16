package ru.baulina.tm.bootstrap;

import ru.baulina.tm.api.repository.ICommandRepository;
import ru.baulina.tm.api.repository.IProjectRepository;
import ru.baulina.tm.api.repository.ITaskRepository;
import ru.baulina.tm.api.repository.IUserRepository;
import ru.baulina.tm.api.service.*;
import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.command.auth.*;
import ru.baulina.tm.command.project.*;
import ru.baulina.tm.command.system.*;
import ru.baulina.tm.command.task.*;
import ru.baulina.tm.enumerated.Role;
import ru.baulina.tm.exception.system.EmptyArgumentException;
import ru.baulina.tm.exception.system.UnknownCommandException;
import ru.baulina.tm.repository.CommandRepository;
import ru.baulina.tm.repository.ProjectRepository;
import ru.baulina.tm.repository.TaskRepository;
import ru.baulina.tm.repository.UserRepository;
import ru.baulina.tm.service.*;
import ru.baulina.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private static final Class[] COMMANDS = new Class[] {
        HelpCommand.class, AboutCommand.class, InfoCommand.class, VersionCommand.class, ArgumentShowCommand.class,
        CommandShowCommand.class, ExitCommand.class,

        TaskClearCommand.class, TaskCreateCommand.class, TaskShowCommand.class, TaskRemoveByIdCommand.class,
        TaskRemoveByIndexCommand.class, TaskRemoveByNameCommand.class, TaskShowByIdCommand.class,
        TaskShowByIndexCommand.class, TaskShowByNameCommand.class, TaskUpdateByIdCommand.class,
        TaskUpdateByIndexCommand.class,

        ProjectClearCommand.class, ProjectCreateCommand.class, ProjectShowCommand.class, ProjectRemoveByIdCommand.class,
        ProjectRemoveByIndexCommand.class, ProjectRemoveByNameCommand.class, ProjectShowByIdCommand.class,
        ProjectShowByIndexCommand.class, ProjectShowByNameCommand.class, ProjectUpdateByIdCommand.class,
        ProjectUpdateByIndexCommand.class,

        LoginCommand.class, LogoutCommand.class, UserLockCommand.class, UserUnlockCommand.class,
        PasswordChangeCommand.class, ProfileOfUserCommand.class, ProfileOfUserChangeCommand.class,
        RegistryCommand.class, UserShowCommand.class, UserRemoveCommand.class
    };

    {
        for (final Class clazz: COMMANDS) {
            try{
                final Object commandInstance = clazz.newInstance();
                final AbstractCommand command = (AbstractCommand) commandInstance;
                command.setServiceLocator(this);
                commandService.add(command);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void initUsers()  {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    public void run(final String[] args) {
        System.out.println("** Welcome to task manager **");
        System.out.println();
        try {
            if (parseArgs(args)) System.exit(0);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("[FAIL]");
            System.exit(0);
        }
        initUsers();
        process();
    }

    private void process() {
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        return parseArg(arg);
    }

    private boolean parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) throw new EmptyArgumentException();
        final AbstractCommand command = commandService.getByArg(arg);
        if (command == null) throw new UnknownCommandException(arg);
        command.execute();
        return true;
    }

    private boolean parseCommand(final String cmd) {
        if (cmd == null || cmd.isEmpty()) return false;
        final AbstractCommand command = commandService.getByName(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
        return true;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

}
