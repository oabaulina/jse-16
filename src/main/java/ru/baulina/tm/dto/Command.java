package ru.baulina.tm.dto;

public class Command {

    private String name = "";

    private String arg = "";

    private String description = "";

    public Command(final String name, final String arg, final String description) {
        this.name = name;
        this.arg = arg;
        this.description = description;
    }

    public Command(final String name, final String arg) {
        this.name = name;
        this.arg = arg;
    }

    public Command(final String name) {
        this.name = name;
    }

    public Command() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArg() {
        return arg;
    }

    public void setArg(final String arg) {
        this.arg = arg;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();

        if (name != null && !name.isEmpty()) result.append(name);
        if (arg != null && !arg.isEmpty()) result.append(", ").append(arg);
        if (description != null && !description.isEmpty()) result.append(": ").append(description);

        return result.toString();
    }

}
