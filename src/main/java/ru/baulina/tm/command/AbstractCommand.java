package ru.baulina.tm.command;

import ru.baulina.tm.api.service.IServiceLocator;

import ru.baulina.tm.enumerated.Role;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public Role[] roles() {
        return null;
    }

    public abstract String arg();

    public abstract String name();

    public abstract String description();

    public abstract void execute();

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        final String nameCommand = name();
        final String argumentCommand = arg();
        final String descriptionCommand = description();

        if (nameCommand != null && !nameCommand.isEmpty()) result.append(nameCommand);
        if (argumentCommand != null && !argumentCommand.isEmpty()) result.append(", ").append(argumentCommand);
        if (descriptionCommand != null && !descriptionCommand.isEmpty()) result.append(": ").append(descriptionCommand);

        return result.toString();
    }

}
