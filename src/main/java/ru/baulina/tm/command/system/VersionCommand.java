package ru.baulina.tm.command.system;

import ru.baulina.tm.command.AbstractCommand;

public final class VersionCommand extends AbstractCommand {

    final String VERSION_ARG = "-v";

    final String VERSION_NAME = "version";

    final String VERSION_DESCRIPTION = "Display program version.";

    @Override
    public String arg() {
        return VERSION_ARG;
    }

    @Override
    public String name() {
        return VERSION_NAME;
    }

    @Override
    public String description() {
        return VERSION_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.0.15");
        System.out.println("OK");
        System.out.println();
    }

}
