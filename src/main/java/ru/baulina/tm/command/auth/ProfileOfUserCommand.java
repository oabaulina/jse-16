package ru.baulina.tm.command.auth;

import ru.baulina.tm.entity.User;
import ru.baulina.tm.util.TerminalUtil;

public final class ProfileOfUserCommand extends AbstractAuthCommand {

    final String PROFILE_OF_USER_NAME = "profiler-of-user";

    final String PROFILE_OF_USER_DESCRIPTION = "Show user's profiler.";

    @Override
    public String name() {
        return PROFILE_OF_USER_NAME;
    }

    @Override
    public String description() {
        return PROFILE_OF_USER_DESCRIPTION;
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().isAuth();
        System.out.println("[SHOW_PROFILE_OF_USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findByLogin(login);
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("PASSWORD: " + user.getPasswordHash());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("FEST NAME: " + user.getFestName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("[OK]");
        System.out.println("");
    }

}
