package ru.baulina.tm.command.auth;

import ru.baulina.tm.entity.User;

import java.util.List;

public final class UserShowCommand extends AbstractAuthCommand {

    final String LIST_USERS_NAME = "list-users";

    final String LIST_USERS_DESCRIPTION = "Show users.";

    @Override
    public String name() {
        return LIST_USERS_NAME;
    }

    @Override
    public String description() {
        return LIST_USERS_DESCRIPTION;
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().isAuth();
        final List<User> users = serviceLocator.getUserService().findAll();
        System.out.println("[SHOW_LIST_USERS]");
        int index = 1;
        for (User user: users) {
            System.out.println(index + ". ");
            System.out.println("LOGIN: " + user.getLogin());
            System.out.println("PASSWORD: " + user.getPasswordHash());
            System.out.println("E-MAIL: " + user.getEmail());
            System.out.println("FEST NAME: " + user.getFestName());
            System.out.println("LAST NAME: " + user.getLastName());
            index++;
        }
        System.out.println("[OK]");
        System.out.println("");
    }

}
