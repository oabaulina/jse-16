package ru.baulina.tm.command.auth;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.util.TerminalUtil;

public final class RegistryCommand extends AbstractCommand {

    final String REGISTRY_NAME = "registry";

    final String REGISTRY_DESCRIPTION = "Register new users.";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return REGISTRY_NAME;
    }

    @Override
    public String description() {
        return REGISTRY_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER E-MAIL:");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
        System.out.println("[OK]");
        System.out.println("");
    }

}
