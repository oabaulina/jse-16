package ru.baulina.tm.command.auth;

import ru.baulina.tm.command.AbstractCommand;

public final class LogoutCommand extends AbstractCommand {

    final String LOGOUT_NAME = "logout";

    final String LOGOUT_DESCRIPTION = "Log out.";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return LOGOUT_NAME;
    }

    @Override
    public String description() {
        return LOGOUT_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
        System.out.println("[OK]");
    }

}
