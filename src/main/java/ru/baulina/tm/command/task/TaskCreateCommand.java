package ru.baulina.tm.command.task;

import ru.baulina.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    final String TASK_CREATE_NAME = "task-create";

    final String TASK_CREATE_DESCRIPTION = "Create new task.";

    @Override
    public String name() {
        return TASK_CREATE_NAME;
    }

    @Override
    public String description() {
        return TASK_CREATE_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final  String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final  String description = TerminalUtil.nextLine();
        final Long userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getTaskService().create(userId, name, description);
        System.out.println("[OK]");
        System.out.println();
    }

}
