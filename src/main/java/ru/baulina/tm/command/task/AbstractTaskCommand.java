package ru.baulina.tm.command.task;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.enumerated.Role;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.USER, Role.ADMIN};
    }

}
