package ru.baulina.tm.command.task;

import ru.baulina.tm.entity.Task;
import ru.baulina.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    final String TASK_REMOVE_BY_INDEX_NAME = "task-remove-by-index";

    final String TASK_REMOVE_BY_INDEX_DESCRIPTION = "Remove task by index.";

    @Override
    public String name() {
        return TASK_REMOVE_BY_INDEX_NAME;
    }

    @Override
    public String description() {
        return TASK_REMOVE_BY_INDEX_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nexInt() -1;
        final Long userId = serviceLocator.getAuthService().getUserId();
        final Task task = serviceLocator.getTaskService().removeOneByIndex(userId, index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        System.out.println("");
    }

}
