package ru.baulina.tm.command.task;

public final class TaskClearCommand extends AbstractTaskCommand {

    final String TASK_CLEAR_NAME = "task-clear";

    final String TASK_CLEAR_DESCRIPTION = "Remove all tasks.";

    @Override
    public String name() {
        return TASK_CLEAR_NAME;
    }

    @Override
    public String description() {
        return TASK_CLEAR_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASK]");
        final Long userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getTaskService().clear(userId);
        System.out.println("[OK]");
        System.out.println();
    }

}
