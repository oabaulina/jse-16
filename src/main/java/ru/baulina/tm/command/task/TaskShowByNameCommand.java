package ru.baulina.tm.command.task;

import ru.baulina.tm.entity.Task;
import ru.baulina.tm.util.TerminalUtil;

public final class TaskShowByNameCommand extends AbstractTaskCommand {

    final String TASK_SHOW_BY_NAME_NAME = "task-show-by-name";

    final String TASK_SHOW_BY_NAME_DESCRIPTION = "Show task by name.";

    @Override
    public String name() {
        return TASK_SHOW_BY_NAME_NAME;
    }

    @Override
    public String description() {
        return TASK_SHOW_BY_NAME_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME");
        final String name = TerminalUtil.nextLine();
        final Long userId = serviceLocator.getAuthService().getUserId();
        final Task task = serviceLocator.getTaskService().findOneByName(userId, name);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
        System.out.println("");
    }

}
