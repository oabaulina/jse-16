package ru.baulina.tm.command.project;

import ru.baulina.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    final String PROJECT_CREATE_NAME = "project-create";

    final String PROJECT_CREATE_DESCRIPTION = "Create new project.";

    @Override
    public String name() {
        return PROJECT_CREATE_NAME;
    }

    @Override
    public String description() {
        return PROJECT_CREATE_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final  String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final  String description = TerminalUtil.nextLine();
        final Long userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getProjectService().create(userId, name, description);
        System.out.println("[OK]");
        System.out.println();
    }

}
