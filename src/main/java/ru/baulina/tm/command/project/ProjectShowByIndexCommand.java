package ru.baulina.tm.command.project;

import ru.baulina.tm.entity.Project;
import ru.baulina.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    final String PROJECT_SHOW_BY_INDEX_NAME = "project-show-by-index";

    final String PROJECT_SHOW_BY_INDEX_DESCRIPTION = "Show project by index.";

    @Override
    public String name() {
        return PROJECT_SHOW_BY_INDEX_NAME;
    }

    @Override
    public String description() {
        return PROJECT_SHOW_BY_INDEX_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nexInt() -1;
        final Long userId = serviceLocator.getAuthService().getUserId();
        final Project project = serviceLocator.getProjectService().findOneByIndex(userId, index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
        System.out.println("");
    }

}
