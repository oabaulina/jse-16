package ru.baulina.tm.exception.empty;

public class EmptyLoginException extends RuntimeException{

    public EmptyLoginException() {
        super("Error! Login is empty...");
    }

}
