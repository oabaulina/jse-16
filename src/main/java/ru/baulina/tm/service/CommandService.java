package ru.baulina.tm.service;

import ru.baulina.tm.api.repository.ICommandRepository;
import ru.baulina.tm.api.service.ICommandService;
import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.exception.empty.EmptyNameException;
import ru.baulina.tm.exception.system.EmptyArgumentException;

import java.util.List;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void add(AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @Override
    public void remove(AbstractCommand command) {
        if (command == null) return;
        commandRepository.remove(command);
    }

    @Override
    public List<AbstractCommand> findAll() {
        return commandRepository.findAll();
    }
    @Override
    public List<String> getCommandsNames() {
        return commandRepository.getCommandsNames();
    }

    @Override
    public List<String> getArgs() {
        return commandRepository.getArgs();
    }

    @Override
    public void clear() {
        commandRepository.clear();
    }

    @Override
    public AbstractCommand getByArg(String arg) {
        if (arg == null || arg.isEmpty()) throw new EmptyArgumentException();
        return commandRepository.getByArg(arg);
    }

    @Override
    public AbstractCommand getByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return commandRepository.getByName(name);
    }

    @Override
    public AbstractCommand removeByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return commandRepository.removeByName(name);
    }

    @Override
    public AbstractCommand removeByArg(String arg) {
        if (arg == null || arg.isEmpty()) throw new EmptyArgumentException();
        return commandRepository.removeByArg(arg);
    }

}
