package ru.baulina.tm.service;

import ru.baulina.tm.api.repository.IProjectRepository;
import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.exception.empty.EmptyIdException;
import ru.baulina.tm.exception.empty.EmptyPasswordException;
import ru.baulina.tm.exception.incorrect.IncorrectIndexException;
import ru.baulina.tm.exception.empty.EmptyNameException;
import ru.baulina.tm.entity.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final Long userId, final String name) {
        if (userId == null || userId > 0) throw new EmptyPasswordException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(userId, project);

    }

    @Override
    public void create(final Long userId, final String name, final String description) {
        if (userId == null || userId > 0) throw new EmptyPasswordException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
    }

    @Override
    public void add(final Long userId, final Project project) {
        if (userId == null || userId > 0) throw new EmptyPasswordException();
        if (project == null) return;
        projectRepository.add(userId, project);
    }

    @Override
    public void remove(final Long userId, final Project project) {
        if (userId == null || userId > 0) throw new EmptyPasswordException();
        if (project == null) return;
        projectRepository.remove(userId, project);
    }

    @Override
    public List<Project> findAll(final Long userId) {
        if (userId == null || userId > 0) throw new EmptyPasswordException();
        return projectRepository.findAll(userId);
    }

    @Override
    public void clear(final Long userId) {
        if (userId == null || userId > 0) throw new EmptyPasswordException();
        projectRepository.clear(userId);
    }

    @Override
    public Project findOneById(final Long userId, final Long id) {
        if (userId == null || userId > 0) throw new EmptyPasswordException();
        if (id == null || id < 0) throw new EmptyIdException();
        return projectRepository.findOneById(userId, id);
    }

    @Override
    public Project findOneByIndex(final Long userId, final Integer index) {
        if (userId == null || userId > 0) throw new EmptyPasswordException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.findOneByIndex(userId, index);
    }

    @Override
    public Project findOneByName(final Long userId, final String name) {
        if (userId == null || userId > 0) throw new EmptyPasswordException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findOneByName(userId, name);
    }

    @Override
    public Project removeOneById(final Long userId, final Long id) {
        if (userId == null || userId > 0) throw new EmptyPasswordException();
        if (id == null || id < 0) throw new EmptyIdException();
        return projectRepository.removeOneById(userId, id);
    }

    @Override
    public Project removeOneByIndex(final Long userId, final Integer index) {
        if (userId == null || userId > 0) throw new EmptyPasswordException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Project removeOneByName(final Long userId, final String name) {
        if (userId == null || userId > 0) throw new EmptyPasswordException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeOneByName(userId, name);
    }

    @Override
    public Project updateTaskById(final Long userId, final Long id, final String name, final String description) {
        if (userId == null || userId > 0) throw new EmptyPasswordException();
        if (id == null || id < 0) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneById(userId, id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateTaskByIndex(final Long userId, final Integer index, final String name, final String description) {
        if (userId == null || userId > 0) throw new EmptyPasswordException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByIndex(userId, index);

       if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return null;
    }

}
