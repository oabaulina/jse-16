package ru.baulina.tm.service;

import ru.baulina.tm.entity.User;
import ru.baulina.tm.api.service.IAuthService;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.enumerated.Role;
import ru.baulina.tm.exception.user.AccessDeniedException;
import ru.baulina.tm.exception.empty.EmptyLoginException;
import ru.baulina.tm.exception.empty.EmptyPasswordException;
import ru.baulina.tm.util.HashUtil;

import java.util.List;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private Long userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public Long getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public void checkRole(final Role[] roles) {
        if (roles == null || roles.length == 0) return;
        final Long userId = getUserId();
        final User user = userService.findById(userId);
        final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role item: roles) {
            if (role.equals(item)) return;
        }
        throw new AccessDeniedException();
    }

    @Override
    public void isAuth() {
        if (userId == null) throw new AccessDeniedException();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user.getLocked()) throw new AccessDeniedException();
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(final String login, final String password, final String email) {
        userService.create(login, password, email);
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User currentUser = userService.findById(userId);
        final User userShow = userService.findByLogin(login);
        if (userShow.getId() == userId || currentUser.getLogin().equals("admin")) { return userShow; }
        else { throw new AccessDeniedException(); }
    }

    @Override
    public User findById() {
        return userService.findById(userId);
    }

    @Override
    public void changePassword(final String passwordOld, final String passwordNew) {
        final User user = findById();
        if (user == null) throw new AccessDeniedException();
        final String hashOld = HashUtil.salt(passwordOld);
        if (hashOld == null) throw new AccessDeniedException();
        if (!hashOld.equals(user.getPasswordHash())) throw new AccessDeniedException();
        final String hashNew = HashUtil.salt(passwordNew);
        user.setPasswordHash(hashNew);
    }

    @Override
    public List<User> findAll() {
        final User user = userService.findById(userId);
        if (!user.getLogin().equals("admin")) throw new AccessDeniedException();
        return userService.findAll();
    }

    @Override
    public void changeUser(final String email, final String festName, final String LastName) {
        final User user = findById();
        user.setEmail(email);
        user.setFestName(festName);
        user.setLastName(LastName);
    }
}
