package ru.baulina.tm.util;

import ru.baulina.tm.exception.incorrect.IncorrectIndexException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nexInt() {
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new IncorrectIndexException(value);
        }

    }

    static Long nexLong() {
        final String value = nextLine();
        try {
            return Long.parseLong(value);
        } catch (Exception e) {
            throw new IncorrectIndexException(value);
        }
    }

}
